#!/bin/bash

# To be run as root

# Network configuration
systemctl enable NetworkManager
./macaddress.sh

# System update
dnf update

# Additionnal repos etc.
dnf install -y epel-release
dnf install -y snapd
dnf config-manager --set-enabled PowerTools #Nécessaire à Pandoc
dnf install --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm\n
#Installer le repo Flathub manuellement

# Development tools
dnf groupinstall "Development Tools" -y
dnf install -y gcc cmake make fuse-devel mbedtls-devel ruby-devel

# Start and enable Snap
systemctl start snapd
systemctl enable snapd

# dnf packages
dnf install -y gimp zsh idle3 pandoc ansible gparted keepassxc xdotool ntfs-3g gnome-tweaks terminator chromium mariadb nextcloud-client tigervnc-server tigervnc-server-module gwenview sqlite stellarium liberation-narrow-fonts libimobiledevice usbmuxd rclone ImageMagick virt-viewer libgexiv2 dislocker fuse-dislocker terminator exfat-utils fuse-exfat pidgin pidgin-sipe containerd.io tex poppler htop bash-completion nextcloud-client podman

#Docker
#sudo dnf install docker.io
#sudo dnf install docker


#Gnome components
sudo dnf install gnome-tweaks gnome-shell-extension chrome-gnome-shell

#Tex
#sudo yum install texlive-pdftex texlive-latex-bin texlive-texconfig* texlive-latex* texlive-metafont* texlive-cmap* texlive-ec texlive-fncychap* texlive-pdftex-def texlive-fancyhdr* texlive-titlesec* texlive-multirow texlive-framed* texlive-wrapfig* texlive-parskip* texlive-caption texlive-ifluatex* texlive-collection-fontsrecommended texlive-collection-latexrecommended

#XGD - To be reviewed
#sudo dnf install xgd-desktop-portal-gtk
#sudo dnf install xdg-desktop-portal-gtk
#sudo dnf install xdg-document-portal-gtk
#sudo dnf install xdg-document-portal
#sudo dnf install xdg-desktop-portal


#ZSH
sudo dnf install zsh
sudo dnf install -y zsh-doc
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
#ruby bash_to_zsh_history.rb

#OpenSSL
dnf install openssl-devel libffi-devel bzip2-devel -y

# KDE components
dnf install -y dolphin konsole

# Snap packages
#snap install keepassxc
snap install krita
snap install vlc
snap install panwriter
snap install filezilla --beta
snap install gnome-terminator --beta
#snap install slack --classic
snap install mattermost-desktop --beta
snap install authy --beta
snap install shutter
snap install sqlitebrowser
#snap install nextcloud

# Python components
dnf install -y python3-pillow
pip3 install ipython
pip3 install click
pip3 install xlsxwriter

# Flatpak packages
#flatpak install flathub org.nextcloud.Nextcloud
#flatpak uninstall com.nextcloud.desktopclient.nextcloud

# Latex
yum install texlive-pdftex texlive-latex-bin texlive-texconfig* texlive-latex* texlive-metafont* texlive-cmap* texlive-ec texlive-fncychap* texlive-pdftex-def texlive-fancyhdr* texlive-titlesec* texlive-multirow texlive-framed* texlive-wrapfig* texlive-parskip* texlive-caption texlive-ifluatex* texlive-collection-fontsrecommended texlive-collection-latexrecommended

#Slack
wget https://downloads.slack-edge.com/linux_releases/slack-4.0.2-0.1.fc21.x86_64.rpm
dnf install ./slack-*.rpm
#Fichier de config dépôt à rédiger ou copier : /etc/yum.repos.d/slack.repo

# RPM packages (see in the "Packages" folder)
#rpm -ivh nordvpn-release-1.0.0-1.noarch.rpm
#rpm -ihv VirtualBox-6.1-6.1.4_136177_el8-1.x86_64.rpm
#rpm -ivh master-pdf-editor-5.4.38-qt5.x86_64.rpm
#rpm -ivh veracrypt-1.24-Update4-CentOS-8-x86_64.rpm

# Miscellaneous
#Stellarium en Appimage
