#!/bin/bash

#Ajout de divers codecs multimédias à partir du dépôt RPMFusion. Inclus H.264 et Mpeg4 AAC.  

dnf install --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm #Si pas installé précédemment
dnf install --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm
dnf install gstreamer1-plugins-bad-nonfree x264
dnf install gstreamer1-libav gstreamer1-plugins-ugly
