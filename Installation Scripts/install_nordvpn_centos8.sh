#!/bin/bash

wget -qnc https://repo.nordvpn.com/yum/nordvpn/centos/noarch/Packages/n/nordvpn-release-1.0.0-1.noarch.rpm
yum install nordvpn-release-1.0.0-1.noarch.rpm
rpm -v --import https://repo.nordvpn.com/gpg/nordvpn_public.asc
yum update
dnf install nordvpn
