# My CentOS 8 desktop installation for a professional use

**Tested on a CentOS 8 Rolling Release running on an external SSD on a Thinkpad T580, besides an encrypted Windows 10 installed on the laptop disk. Boot device must be chosen via the laptop UEFI Boot Menu. CentOS is encrypted during the installation process.**

## List of Contents

- [Folders description](#folders-description)
- [List of scripts](#list-of-scripts)
- [Ethernet configuration](#ethernet-configuration)
- [Signing VirtualBox kernel modules in a CentOS 8 installed with UEFI Secure Boot enabled](#signing-virtualbox-kernel-modules-in-a-centos-8-installed-with-enabled-uefi-secure-boot)
- [Automatically starting ssh-agent and adding SSH keys](#automatically-starting-ssh-agent-and-adding-ssh-keys)
- [Customising Gnome](https://gitlab.com/sakura-lain/my-centos8-desktop-professional#customising-gnome)
- [Multimedia codecs](https://gitlab.com/sakura-lain/my-centos8-desktop-professional#multimedia-codecs)
- [Installing and configuring ZSH](#installing-and-configuring-zsh)
- [Decrypting a BitLocker partition](#decrypting-a-bitlocker-partition)
- [Using Rclone to mount your business OneDrive and SharePoint remotes](#using-rclone-to-mount-your-business-onedrive-and-sharepoint-remotes)
- [Sharing the current desktop using VNC](#sharing-the-current-desktop-using-vnc)
- [Miscellanous](#miscellanous)
- [Couldn't install yet (due to missing packages or unsolved dependencies)](#couldnt-install-yet-due-to-missing-packages-or-unsolved-dependencies)

## Folders description

- **"Package & Repositories"** contains packages and repositories configuration files. Must be updated.
- **"Installation Scripts"** contains installation and configuration scripts.
- **"Signing VBox Kernel Modules"** contains scripts to sign VirtualBox kernel modules (see below).
- **"Pictures"** contains installation details, the CentOS logo and a view of the customised Gnome desktop.

## List of Scripts

### Installation scripts

- The main installation script is `postinstall_centos8_professionnal.sh`. It installs several programms including **ZSH**, **Gparted**, **Python tools**, **Ansible**, **Nextcloud**, **VLC**, **The Gimp** or **Filezilla**.
- There are specific scripts to install **Alien**, **Atom**, **Calibre**, **Docker**, **Typora** and **Dislocker**.
- There are scripts to install **Bash completion** and **additional multimedia codecs**.

### Configuration Scripts

There are three configuration scripts that can be used associated with a cron task:
- The first script permits to **decrypt a BitLocker encrypted partition** using Dislocker;
- The second one can **change the machine MAC address**;
- The third one can **mount a OneDrive and SharePoint remote** using Rclone.
- The last one provides an easy way to share your current desktop with another device located on your local network using **VNC**.

### How to use the scripts

These scripts contain links that may need to be updated and lines to be uncommented to add options. Some scripts may be run with sudo, some don't because sudo commands are already in the script. Also beware of the paths to the packages when running them.

To use the scripts, make sure to make them executable :

```
chmod +x /path-to-the-script/script.sh
```

## Ethernet configuration

### Activating the ethernet on boot

- Identify the configuration file corresponding to your ethernet interface in the directory `/etc/sysconfig/network-scripts/`. The file name starts with `ifcfg-` usually followed an interface name beginning with `enp` for an ethernet interface, for instance `ifcfg-enp0s31f6`.
- Open the file with your favorite text editor and set the `ONBOOT` option to `yes`.

### Changing the MAC address

To change the ethernet MAC address to bypass any corporate network limitations, a script is provided to be run after each boot:
```
$ sudo ./Installation\ Scripts/macaddress.sh
```
Alternatively you can try the `macchanger` tool.

### Running the script automatically at reboot

To run the script at each reboot, create a cron task with the root account
- `crontab -e`
- Add the line `@reboot /your_script_path/macaddress.sh`

## Signing VirtualBox kernel modules in a CentOS 8 installed with enabled UEFI Secure Boot

**This [tutorial](https://gist.github.com/reillysiemens/ac6bea1e6c7684d62f544bd79b2182a4) indicates how to proceed.** Additionally, specific SELinux rules have to be set in CentOS.

Launch the script as root:

```
# ./Signing\ VBox\ Kernel\ Modules/sign-vbox-modules.sh
```

Setting up SELinux rules:

```
# ausearch -c "vboxdrv.sh" --raw | audit2allow -M my-vboxdrvsh
# semodule -i my-vboxdrvsh.pp
```

Then:

```
# reboot
```
To be done everytime the kernel is updated.

### Source:
- https://gist.github.com/reillysiemens/ac6bea1e6c7684d62f544bd79b2182a4

### See also:
- https://superuser.com/questions/1438279/how-to-sign-a-kernel-module-ubuntu-18-04
- https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Kernel_Administration_Guide/sect-signing-kernel-modules-for-secure-boot.html

## Setting up Docker

```
$ ./Scripts/install_docker_centos8.sh
```
The system may need to be rebooted afterwards.

### Sources

- https://linuxconfig.org/how-to-install-docker-in-rhel-8)
- https://forums.docker.com/t/docker-ce-on-centos-8/81648/2
- https://www.linuxtechi.com/install-docker-ce-centos-8-rhel-8/
- https://computingforgeeks.com/install-docker-and-docker-compose-on-rhel-8-centos-8/
- https://unix.stackexchange.com/questions/199966/how-to-configure-centos-7-firewalld-to-allow-docker-containers-free-access-to-th

## Automatically starting ssh-agent and adding SSH keys

By default, CentOS 8 doesn't keep the memory of SSH keys after reboot.

Once the keys are copied into `~/.ssh`, a few lines must be added to `.bashrc` or `.zshrc`:
```
if [ -z "$SSH_AUTH_SOCK" ] ; then
 eval `ssh-agent -s`
 ssh-add
fi
```

### Source

- https://superuser.com/questions/1152833/save-identities-added-by-ssh-add-so-they-persist

## Customising Gnome

### Main theme

Edricus theme for Le Garage numérique:
- https://gitlab.com/garagenum/edricus-gnome

The GUI fot Gnome Tweaks must be installed separately:
```
$ sudo dnf install gnome-tweaks
```

The classic Adwaita icon theme is kept.

### Arc Menu

Arc Menu was installed via the Firefox extension Gnome Shell Integration. Arc Menu icon is replaced by CentOS logo via Arc Menu settings. The CentOS logo can be found in the "Pictures" folder.

More on Arc Menu :
- https://extensions.gnome.org/extension/1228/arc-menu/
- https://gitlab.com/LinxGem33/Arc-Menu

### Final result

![](Pictures/centos8_customised_gnome_desktop.png)

## Multimedia codecs

Certain multimedia codecs might be missing, even with VLC. It is the case for instance of H.264 and Mpeg4 AAC codecs.

To install them, please refer to the script `multimedia-codecs.sh`

### Sources

- https://lib.frama.site/audio/fedora_codecs
- https://rpmfusion.org/Configuration/

## Installing and configuring ZSH

Please see [my ZSH config](https://gitlab.com/sakura-lain/my-zsh-config).

## Decrypting a BitLocker partition

### On Windows side

#### OneDrive settings
- Make all your OneDrive files locally available to be able to view them on Linux. To do so, first click on the OneDrive icon in the systray the select "Help & settings" > "Settings" :

![](Pictures/onedrive-1.png)
- Secondly, in the "Settings" tab, untick the box "Save space and download files as you use them" in the section "Files on demand":

![](Pictures/onedrive-2.png)

#### Bitlocker settings
- Find your Bitlocker key in Windows, using the tool available in the control panel:
![](Pictures/bitlocker-1.png)
- Choose the option that allows you to save your bitlocker key:
![](Pictures/bitlocker-2.png)
- Save it in a file :

![](Pictures/bitlocker-3.png)

**/!\Beware: the file cannot be read in Linux.** Please copy its content in another text or markdown file to do so.

### On Linux side

- The version provided by the DNF packages manager being a bit buggy, the latest version of [Dislocker](https://github.com/Aorimn/dislocker) must be installed through compilation.
- An installation script as well as a decryption script are provided. You must set your own Bitlocker key in it then run it using `sudo su -`.
- Once it's run, the Windows partition should also be available via Nautilus or any other graphic files explorer.

### Sources

- https://support.microsoft.com/en-us/office/sync-files-with-onedrive-files-on-demand-62e8d748-7877-420f-b600-24b56562aa70
- https://support.microsoft.com/fr-fr/help/4530477/windows-10-finding-your-bitlocker-recovery-key
- https://github.com/Aorimn/dislocker
- https://www.leshirondellesdunet.com/dislocker-recuperer-les-donnes-chiffres-avec-bitlocker
- https://theevilbit.blogspot.com/2014/04/using-dislocker-to-mount-bitlocker.html
- https://github.com/Aorimn/dislocker/issues/185
- https://forum.level1techs.com/t/problem-mounting-bitlocker-nvme-with-dislocker-on-linux/157096

## Using Rclone to mount your business OneDrive and SharePoint remotes

Rclone is a useful tool to be able to read and write OneDrive and SharePoint files sharings (and many other cloud sharings). It is particularly helpful if you cannot access to your cloud files on the decrypted BitLocker partition even as root, due to a rights issue (because your files belong to your corporation). Moreover, the primary goal of Rclone is to be a synchronisation tool which is a really appreciated functionality compared to a simple "mount".

### Common steps

Here's how to proceed to install and configure Rclone for OneDrive and SharePoint:

- Install Rclone : `sudo dnf install rclone`. 
- Launch Rclone: `rclone config`.
- Choose "New remote" (option `n`).
- Choose a name for your remote. Let's say you call it "onedrive" or "sharepoint".
- Type the number corresponding to "Microsoft OneDrive".
- Leave blank the fields asking for "client_id" and "client_secret".
- Answer "no" (`n`) to the question "Edit advanced config?".
- Answer "yes" (`y`) to the question "Use auto config?". This should open your default browser and either check your connection or invite you to connect to your account to check it.

Then the procedure differs a bit depending wether you want to connect a OneDrive or a SharePoint remote. Please see the steps for each configuration below.

### To connect a OneDrive Business cloud
- Choose "OneDrive Personal or Business".
- You'll see an account. Check and validate if it's the one you want to connect to.
- Check if the proposed drive is correct.
- Check if "access_token", "drive_id" and "drive_type" are correct. If so, choose "yes" (`y`).
- You can either quit the programm (`q`) or configure a new remote (`n`) amongst other options.

### To connect a SharePoint site
- Either choose "Search a Sharepoint site" or "Type in SiteID". Both options should work well on a CentOS 8, while the other is the only one to work on an Ubuntu 20.04 for instance.
- If you choose "Search a Sharepoint site", enter the name of your SharePoint site, the programm should find it. Check if everything is OK and validate (`y`).
- If you choose "Type in SiteID", fisrt find you site ID. To do so go to the page https://yoursharepointname.sharepoint.com/sites/yoursitename/_api/site/id (change "yourwharepointname" and "yoursitename" for the the values corresponding to your Sharepoint site).
- Your site ID is the first series of letters and numbers displayed, just before the words "(function (data)".
- Enter it and check if everything is OK. If so, validate (`y`).
- You can either quit the programm (`q`) or configure a new remote (`n`) amongst other options.

### Mounting the disks

- To be able to mount the disks, you must create a dedicated directory. For instance, for a OneDrive cloud to be mounted in your home directory :
```
mkdir ~/OneDrive
```
- Mount the disk with the following command. This will mount the remote named "onedrive" on the directory `~/OneDrive`:
```
rclone --vfs-cache-mode writes mount onedrive: ~/OneDrive
```
- Follow the same steps to mount a SharePoint remote:
```
mkdir ~/SharePoint
rclone --vfs-cache-mode writes mount sharepoint: ~/SharePoint
```
A script is provided that you can use associated with a cron task to auto-mount the disks at boot. it is not necessary to be root to do so.

### Sources

- https://www.linuxuprising.com/2018/07/how-to-mount-onedrive-in-linux-using.html
- https://rclone.org/onedrive/
- https://blog.jonmassey.co.uk/posts/sharepoint-rclone/
- https://korben.info/rclone-rsync-cloud-dropbox-amazon-s3-google-drive-hubic-backblaze-etc.html

## Sharing the current desktop using VNC

### VNC installation and configuration
- First install TigerVNC:
```
dnf install tigervnc-server tigervnc-server-module -y
```
- Set a password (this will create `~/.vnc/passwd`):
```
vncpasswd
```

### Graphical interface configuration
- Uncomment the line `WaylandEnable=false` from the file `/etc/gdm/custom.conf` so that remote desktop session request via VNC is handled by xorg of GNOME desktop in place of wayland display manager, since Wayland is not configured to handled remote rendering API like Xorg.
- Edit `/etc/tigervnc/vncserver-config-defaults` and add `session=gnome`

### SELinux Configuration
To make VNC work with SELinux, you need to install a module and apply the right SELinux context on a few files:
```
semodule -i /usr/share/selinux/packages/vncsession.pp
restorecon /usr/sbin/vncsession /usr/libexec/vncsession-start
```
If you already have VNC installed, you need to remove `$HOME/.vnc folder` if it exists and recreate again with `vncpasswd`. This is needed to have `.vnc` folder with correct SELinux context. Alternatively you can use: `restorecon -RFv $HOME/.vnc`

### Launch the desktop sharing
```
x0vncserver -PasswordFile=.vnc/passwd -AlwaysShared=1
```

### Open the ports in firewalld
This configuration allows VNC screen sharing on devices located on the local network only:
```
sudo firewall-cmd --add-rich-rule='rule family="ipv4" source address="192.168.1.0/24" service name=vnc-server accept'
```
Add `--permanent` to make the rule permanent.

A script is provided to launch the desktop and open the ports.

### Sources
- https://www.linuxtechi.com/install-configure-vnc-server-centos8-rhel8/
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/ch-tigervnc#sec-vnc-sharing-an-existing-desktop

## Miscellanous

- Markdown Preview Plus is added to Atom.

## Couldn't install yet (due to missing packages or unsolved dependencies)

- Shutter
- PDFShuffler/PDFArranger
- Master PDF Editor
- Synology Cloud Station
