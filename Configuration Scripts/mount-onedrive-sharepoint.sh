#!/bin/bash

#You can change the names of remotes and directories according to your needs.

if [ ! -d ~/OneDrive ]; then
	mkdir ~/OneDrive
fi
if [ ! -d ~/SharePoint ]; then
	mkdir ~/SharePoint
fi

sh -c "rclone --vfs-cache-mode writes mount onedrive: ~/OneDrive" &
sh -c "rclone --vfs-cache-mode writes mount sharepoint: ~/SharePoint" &
