#!/bin/bash

#sudo su -

if [ ! -d /mnt/bitlocker ]; then
	mkdir /mnt/bitlocker
fi
if [ ! -d /mnt/windows ]; then
	mkdir /mnt/windows
fi

dislocker -v -V /dev/nvme0n1p4 --recovery-password=your-bitlocker-key -- /mnt/bitlocker
mount -r -o loop /mnt/bitlocker/dislocker-file /mnt/windows
