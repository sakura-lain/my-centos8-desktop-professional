 #!/bin/bash 

 # Source: https://superuser.com/questions/1438279/how-to-sign-a-kernel-module-ubuntu-18-04
 
 for modfile in $(dirname $(modinfo -n vboxdrv))/*.ko; do 
    echo "Signing $modfile" 
    /usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 \ 
                                                     /root/module-signing/MOK.priv \ 
                                                     /root/module-signing/MOK.der "$modfile" 
 done 
